import itertools
from enum import Enum
from random import randrange

from django.contrib.auth.models import User
from django.contrib.postgres.fields import (
    ArrayField,
    JSONField,
)
from django.db import models

import numpy as np
from hashid_field import HashidField


class Game(models.Model):
    owner = models.ForeignKey(User, related_name="games", on_delete=models.DO_NOTHING, null=True)
    width = models.IntegerField(default=16)
    height = models.IntegerField(default=16)
    mines = models.IntegerField(default=20)
    board = JSONField(null=True)

    def __str__(self):
        return f'{self.id}'

    def _calculate_adjacent(self, x, y, board):
        adjacent_mines = 0
        for x_offset in range(-1, 2):
            for y_offset in range(-1, 2):
                # skip current cell
                if x_offset == 0 and y_offset == 0:
                    continue
                new_x, new_y = (x + x_offset, y + y_offset)
                if (new_x >= 0) and (new_x <= self.width - 1):
                    if (new_y >= 0) and (new_y <= self.height - 1):
                        if 'mine' in board[new_x][new_y]:
                            adjacent_mines += board[new_x][new_y]['mine']
                        
        
        return adjacent_mines

    def _generate_board(self):
        cell_schema = {
            'mine': False,
            'clear': False,
            'adj': False,

        }
        # Create matrix and then reshape it
        all_cells = np.dstack(np.meshgrid(np.arange(self.width), np.arange(self.height))).reshape(-1, 2)
        # Grab n number of unique random cells
        mine_cells = all_cells[np.random.choice(np.arange(all_cells.shape[0]), size=self.mines, replace=False)]
        board = [[{} for x in range(self.width)] for y in range(self.height)]

        #Start board with mine cells
        for (x, y) in mine_cells:
            try:
                board[x][y] = {**cell_schema, **{'mine': True}}
            except IndexError as e:
                pass
                # handle rectangular inputs

        # Add other cells and calculate adjacency
        for x in range(self.width):
            for y in range(self.height):
                # skip calculating adjacency for mine cells
                if not 'mine' in board[x][y]:
                    board[x][y] = {**cell_schema, **{'adj': self._calculate_adjacent(x, y, board)}}
                board[x][y].update({'x': x, 'y': y})

        return board
            

class Player(models.Model):
    user = models.ForeignKey(User, blank=True, on_delete=models.CASCADE)
    game = models.ForeignKey(Game, related_name='players', on_delete=models.CASCADE)
    score = models.IntegerField()
    flags = JSONField(null=True)

    def __str__(self):
        return f'Player {self.id}'
