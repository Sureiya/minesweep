import React, { Component } from 'react';
import Tile from './tile';

export default class Game extends Component {
    constructor(props) {
        super(props);
        this.state = {
            id: props.game.id,
            width: props.game.width,
            height: props.game.height,
            mines: props.game.mines,
            board: JSON.parse(props.game.board),
        };
        
    }

    onTileClick (e, tile) {
        if(this.state.board[tile.x][tile.y]['mine']) {
            this.state.board[tile.x][tile.y]['clear'] = true;
            // Handle bomb event
        }
        else {
            this.state.board[tile.x][tile.y]['clear'] = true;
            // Clear adjacent free tiles
        }
        return this.state.board[tile.x][tile.y];
    }
    renderBoard() {
        var tileRows = []
        var key = 0;
        this.state.board.forEach((tileRow) => {
            var tiles = [];
            tileRow.forEach((tile) => {
                tiles.push(<Tile 
                    onClick={(props) => this.onTileClick({...props}, tile)}
                    key={`${tile.x} ${tile.y}`} tile={tile} 
                    />);
            });
            tileRows.push(<div className="tile-column">{tiles}</div>);
            key += 1;
        })

        return tileRows;
    }
    render () {
        const { id, board} = this.state
        if (board && id) return this.renderBoard();
    }
}