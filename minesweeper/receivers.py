from django.db.models.signals import post_save
from django.dispatch import receiver

from .models import Game


@receiver(post_save, sender=Game)
def handle_game_save(sender, instance, created, **kwargs):
    if created:
        instance.board = instance._generate_board()
        instance.save()
        

# @receiver(post_save, sender=Board)
# def handle_board_save(sender, instance, created, **kwargs):
#     if created:
#         cells = instance._generate_board(instance.game.width, instance.game.height, instance.game.mines)
#         instance.cells.bulk_create(cells)
