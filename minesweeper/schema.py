import json

from django.contrib.auth.models import User

import graphene
from graphene import ObjectType, relay
from graphene_django.filter.fields import (
    DjangoFilterConnectionField,
)
from graphene_django.types import DjangoObjectType
from graphql_relay.node.node import from_global_id

from . import models


class AddGame(relay.ClientIDMutation):
    
    class Input:
        width = graphene.Int()
        height = graphene.Int()
        mines = graphene.Int()

    game = graphene.Field(lambda: GameNode)

    @classmethod
    def mutate_and_get_payload(cls, root, info, *args, **kwargs):
        (width, height, mines) = (kwargs.get('width'), kwargs.get('height'), kwargs.get('mines'))
        game = models.Game.objects.create(width=width, height=height, mines=mines)
        return AddGame(game=game)


class GameNode(DjangoObjectType):
    class Meta:
        model = models.Game
        interfaces = (relay.Node, )
        filter_fields = {
            'id': ['exact'],
        }

class PlayerNode(DjangoObjectType):
    class Meta:
        model = models.Player
        interfaces = (relay.Node, )
        filter_fields = {
            'id': ['exact'],
        }




class Mutations(graphene.ObjectType):
    add_game = AddGame.Field()


class Query(graphene.ObjectType):

    game = relay.Node.Field(GameNode)
    all_games = DjangoFilterConnectionField(GameNode)

    player = relay.Node.Field(PlayerNode)
    all_players = DjangoFilterConnectionField(PlayerNode)


schema = graphene.Schema(query=Query, mutation=Mutations)
