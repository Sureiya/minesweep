# Generated by Django 2.1.4 on 2018-12-16 02:53

import django.contrib.postgres.fields.jsonb
from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('minesweeper', '0001_initial'),
    ]

    operations = [
        migrations.AlterField(
            model_name='game',
            name='board',
            field=django.contrib.postgres.fields.jsonb.JSONField(null=True),
        ),
    ]
