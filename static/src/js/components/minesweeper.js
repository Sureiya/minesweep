import React, { Component } from 'react';
import { QueryRenderer, graphql } from 'react-relay';
import environment from '../environment';
import Game from './game';

const LoadGame = graphql`
    query minesweeperGameQuery($id: ID!) {
        game(id: $id) {
            id
            width
            height
            mines
            board
        }
    }`;


export default class Minesweeper extends Component {
    constructor(props) {
        super(props);
        this.state = {
            id: props.id || props.match.params.id,
            game: null,
        };
    }

    loadGame() {
        const { game, id } = this.state;

        return (
            <QueryRenderer 
                environment={environment}
                query={LoadGame}
                variables={{id: id}}
                render={({error, props}) => {
                    if (error) {
                        return <div>Error!</div>;
                      }
                      if (!props) {
                        return <div>Loading...</div>;
                      }
                      return (
                          <div className="minesweeper">
                              <Game game={props.game} />
                          </div>
                      );
                }}
                />
        );
    }
    render() {
        if (!this.state.game || state.id != this.state.game.id){
            return this.loadGame();
        }
    }
}
