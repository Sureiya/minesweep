import React from 'react';
import { Route } from 'react-router-dom';
import update from 'immutability-helper';

import {
  Button,
  Collapse,
  Navbar,
  NavbarToggler,
  NavbarBrand,
  Nav,
  NavItem,
  UncontrolledDropdown,
  DropdownToggle,
  DropdownMenu,
  Form,
  FormGroup,
  Label,
  Col,
  Input } from 'reactstrap';
  
  export default class SiteNav extends React.Component {
    constructor(props) {
      super(props);
      this.push = props.push;
      this.toggle = this.toggle.bind(this);
      this.gameOptionsHandler = props.gameOptionsHandler;
      this.newGameHandler = props.newGameHandler;
      this.state = {
        isOpen: false,
        game_options: {
          board_size: 16,
          mines: 20
        }
      };
    }
    
    toggle() {
      this.setState({
        isOpen: !this.state.isOpen
      });
    }

    updateGameOptions(game_options) {
      let newOptions = update(this.state, {
        game_options: {$merge: game_options}});
      this.setState(newOptions);
    }
    
    render() {
      return (
        <div>
          <Navbar light expand="md">
            <NavbarBrand>rdsweep</NavbarBrand>
            <NavbarToggler onClick={this.toggle} />
            <Collapse isOpen={this.state.isOpen} navbar>
              <Nav className="ml-auto" navbar>
                <NavItem>
                <Route render={(context) => (
                      <Button onClick={(props) => this.newGameHandler({...props}, context)} color="primary" size="md" className="new-game">New Game</Button>
                )} />
                
                </NavItem>
                <UncontrolledDropdown nav inNavbar>
                <DropdownToggle nav caret>
                Game Options
                </DropdownToggle>
                <DropdownMenu right>
                <Form inline id="game-form" className=".flex-column flex-column">
                {/* <FormGroup row className="m-2">
                  <Label for="width"  sm={4}>Width</Label>
                  <Col sm={6}>
                    <Input onChange={e => this.gameOptionsHandler('width', e.target.value)} defaultValue="16" type="number" id="width" />
                  </Col>
                </FormGroup> */}
                {/* <FormGroup row className="m-2">
                  <Label for="height"  sm={4}>Height</Label>
                  <Col sm={6}>
                    <Input onChange={e => this.gameOptionsHandler('height', e.target.value)} defaultValue="16"type="number" id="height" />
                  </Col>
                </FormGroup> */}
                <FormGroup row className="m-2">
                  <Label for="board-size"  sm={4}>Board Size</Label>
                  <Col sm={6}>
                    <Input onChange={e => this.updateGameOptions({board_size: e.target.value})} defaultValue="16" type="number" id="board-size" />
                  </Col>
                </FormGroup>
                <FormGroup row className="m-2">
                  <Label for="mines"  sm={4}>Mines</Label>
                  <Col sm={6}>
                    <Input onChange={e => this.updateGameOptions({mines: e.target.value})} defaultValue="20" type="number" id="mines" />
                  </Col>
                </FormGroup>  
              </Form>
                </DropdownMenu>
                </UncontrolledDropdown>
              </Nav>
            </Collapse>
          </Navbar>
        </div>
        );
      }
    }