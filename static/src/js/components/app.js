import React, { Component } from 'react';
import Nav from './nav';
import { BrowserRouter as Router, Route, withRouter, Switch } from 'react-router-dom';
import Minesweeper from './minesweeper';
import { graphql } from 'relay-runtime';
import commitMutation from 'relay-commit-mutation-promise/compat';
import environment from '../environment';

const NewGame = graphql`
    mutation appGameMutation($input: AddGameInput!){
        addGame(input: $input) {
            clientMutationId
            game {
                id
                width
                height
                mines
                board
            }     
            }
        }`;

class App extends Component {
    constructor(props) {
        super(props);
        this.state = {
            id: props.match.params.id,
            game: props.game || null
        };
        
    }

    gameOptionsHandler(type, value) {
        if(type) () => this.state.game_options[type] = value;

    }

    newGameHandler(e, context) {
        let input = {
            input: {
                width: this.state.game_options.board_size,
                height: this.state.game_options.board_size,
                mines: this.state.game_options.mines
            }
        };
        
        commitMutation(environment, {mutation: NewGame, variables: input})
            .then(response => {
                const { id } = response.addGame.game;
                context.history.push(`/game/${id}`);
            })
            .catch(error => {
                console.log(error);
            });
    }

    render() {
        return (
            <div>
            <Router>
                <div>
                    <Nav newGameHandler={this.newGameHandler} gameOptionsHandler={this.gameOptionsHandler} />
                    <Switch>
                        <Route path="/" exact />
                        <Route 
                            path="/game/:id" 
                            render={(props) => <Minesweeper 
                                {...props} 
                                id={this.state.id} 
                                width={this.state.width} 
                                height={this.state.height} 
                                mines={this.state.mines} 
                                />} 
                            />
                    </Switch>
                </div>     
            </Router>
            </div>
        );
    }
}

export default withRouter(App);