# minesweeper Server

...

## Getting Started

Make sure you have pipenv installed as well as python3.7+
```
pipenv install (or pipenv install --dev)
npm install
createdb minesweeper
./manage.py migrate
npm run build:schema
npm run build:relay
npm run dev
```

Now you can open http://localhost:3000/ _note the port `3000` instead of `8000`.


## Deployment

...