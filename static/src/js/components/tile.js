import React from 'react';
import classSet from 'react-classset';

export default class Tile extends React.Component {
    constructor(props) {
        super(props);
        this.state = props.tile;
    };

    onClick(e, data) {
        let tile = this.props.onClick(e, data);
        this.forceUpdate();
    }
    render () {
        var classes = {
            'tile': true,
            'cleared': this.state.clear,
            'mine': this.state.clear && this.state.mine,
        }
        classes[`adj-${this.state.adj}`] = this.state.clear && this.state.adj;
        classes = classSet(classes);

        return (<div className="tile-row"><div className={classes} 
            onClick={(props) => this.onClick({...props})} ></div></div>);
    }
}