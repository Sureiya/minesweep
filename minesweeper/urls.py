from django.conf import settings
from django.conf.urls.static import static
from django.contrib import admin
from django.urls import include, path, re_path
from django.views.decorators.csrf import csrf_exempt
from django.views.generic import TemplateView

from graphene_django.views import GraphQLView

urlpatterns = [
    #path("", TemplateView.as_view(template_name="homepage.html"), name="home"),
    path("", TemplateView.as_view(template_name="game.html"), name="game"),
    re_path(r"^game/*.", TemplateView.as_view(template_name="game.html"), name="game"),
    path("admin/", admin.site.urls),

    path("account/", include("account.urls")),
    path("graphql", csrf_exempt(GraphQLView.as_view(graphiql=True))),
]

urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
